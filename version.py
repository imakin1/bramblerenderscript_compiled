version = '2025-02-20.003'
files = [
    'main.py',
    'reusablemakin.py',
    'bramblerender.py',
    'bramblerender_max2023.py',
    'librarymakin.py',
    'version.py',
    'pack.py',
]
def version_value(datestring=version):
    year,month,day = datestring.split('-')
    v = int(year)*1000+int(month)*31+float(day)
    return v







